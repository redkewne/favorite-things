from source.factory.utilities import random_string
from source.models.models import db, FavoriteThing


class Factory:

    @classmethod
    def setup(cls):
        favorite_thing = cls.create_favorite_thing()
        db.session.add(favorite_thing)
        db.session.commit()

        print(cls.count_rows_in_table())
        print([i.title for i in db.session.query(FavoriteThing).all()])

    @classmethod
    def teardown(cls):
        db.session.query(FavoriteThing).delete()
        db.session.commit()

    @classmethod
    def add_favorite_thing_to_db(cls):
        db.session.add(cls.create_favorite_thing())
        db.session.commit()

    @classmethod
    def create_favorite_thing(cls):
        title = random_string()
        description = random_string()
        ranking = cls.count_rows_in_table() + 1
        audit_log = "[{'message':'hello, world'}]"

        return FavoriteThing(title=title,
                     description=description,
                     ranking=ranking,
                     audit_log=audit_log)

    @classmethod
    def count_rows_in_table(cls):
        return db.session.query(FavoriteThing).count()