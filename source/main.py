from socket import gethostname
from source.app import app
from source.views import views


if __name__ == '__main__':
    if 'liveconsole' not in gethostname():
        app.run()
