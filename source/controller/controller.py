ids = [str(i) for i in range(11)]

def get_all():
    return ids

def get_from_id(todo_id):
    if not todo_id:
        return get_all()
    return todo_id in ids

def edit_from_id(data):
    print('Thing edited')

    return data

def create(data):
    print('Thing created')
    return {data}

def delete(todo_id):
    print('Thing deleted')
    return get_all()
