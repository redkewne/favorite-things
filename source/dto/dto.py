from source.app import ma, db
from source.models.models import FavoriteThing


class FavoriteThingSchema(ma.ModelSchema):
    class Meta:
        model = FavoriteThing

def serialize():
    hobbies = db.session.query(FavoriteThing).all()
    print(FavoriteThingSchema(many=True).dump(hobbies).data)


serialize()