from source.app import db


class FavoriteThing(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String, unique=True, nullable=False)
    description = db.Column(db.Text, unique=False, nullable=True)
    ranking = db.Column(db.Integer, unique=True, nullable=False)
    meta_data = db.Column(db.String, unique=False, nullable=True)
    created_date = db.Column(db.DateTime, unique=False, nullable=True)
    modified_date = db.Column(db.DateTime, unique=False, nullable=True)
    audit_log = db.Column(db.String, unique=False, nullable=False)
    category_id = db.Column(db.Integer, db.ForeignKey('category.id'))

class Category(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=True, nullable=True)
    favorite_things = db.relationship('FavoriteThing', backref='category', lazy='dynamic')

db.create_all()