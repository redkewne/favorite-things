from source.factory.factory import Factory
from source.models.models import db, FavoriteThing


class TestFavoriteThingModel:
    """ Will need proper setup and teardown methods here """

    @staticmethod
    def test_create_single_favorite_thing():
        Factory.teardown()

        favorite_thing = Factory.create_favorite_thing()
        db.session.add(favorite_thing)
        db.session.commit()
        assert FavoriteThing.query.filter_by(title=favorite_thing.title).first().title == favorite_thing.title


    @staticmethod
    def test_delete():
        Factory.teardown()
        assert FavoriteThing.query.count() == 0

