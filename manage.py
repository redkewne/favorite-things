""" Helper module used to get system-wide information. """

import sys, os
from source.factory.factory import Factory


def tests():
    """ Run tests """
    os.system("pytest source")

def run_linter():
    os.system("pylint source --disable=missing-docstring,no-member,invalid-name")

def factory_setup():
    Factory.setup()
    print('Factory setup completed successfully.')

def create_and_add_favorite_thing():
    Factory.add_favorite_thing_to_db()
    print('FavoriteThing created')

def count_rows():
    rows = Factory.count_rows_in_table()
    print(rows, 'rows')

def factory_teardown():
    Factory.teardown()
    print('Factory teardown completed successfully.')

def manage():
    """ Manage system-wide functionality """

    commands = {'tests': tests,
                'lint': run_linter,
                'setup': factory_setup,
                'teardown': factory_teardown,
                'create': create_and_add_favorite_thing,
                'count': count_rows}


    if len(sys.argv) == 2:
        commands[sys.argv[1]]()


if __name__ == '__main__':
    manage()
